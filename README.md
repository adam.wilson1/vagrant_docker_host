### Docker demo box

A quick demonstration box using the following tools:

* Vagrant to initialise the base VM.
* Ansible to configure the base VM as a docker-host.
* Docker to  run containers on.

## Use

This has been thrown together quickly as a demonstration of infrastructure
as code and some DevOps tools working together.